package co.net.une.ejb43.util;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: ServiciosException.java
##	Contenido: Clase que implementa excepciones tipificadas
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
/**
 * Clase que implementa excepciones tipificadas
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class ServiciosException extends Exception {
	
	private static final long serialVersionUID = 1L;
	//mensaje de la excepci�n
	private String mensaje;

	//constructor
	public ServiciosException(String mensaje) {
		super();
		this.mensaje = mensaje;
	}

	//constructor
	public ServiciosException() {
		super();
		this.mensaje = null;
	}
	
	
	/**
	 * Retorna el mensaje tificado a la excepci�n
	 * @return
	 * 		mensaje de la excepci�n
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * Asigna el mensaje tificado a la excepci�n
	 * @param mensaje
	 * 			Mensaje de la excepci�n a asignar
	 */
	public void setMensage(String mensaje) {
		this.mensaje = mensaje;
	}

	
}
