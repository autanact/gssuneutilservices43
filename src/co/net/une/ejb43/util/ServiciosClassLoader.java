package co.net.une.ejb43.util;

/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Servicios WEB GSS SmallWorld 4.3
##	Archivo: ServiciosClassLoader.java
##	Contenido: Clase que contiene las propiedades iniciales de los Servicios WEB. Sigue el patron de dise�o Singleton
##  Condiciones: El archivo de configuraci�n tiene que estar en la misma carpeta donde se despliegue el archivo GSSUNEUtilServices43.jar
##	Autor: Freddy Molina
##  Fecha creaci�n: 19-02-2016
##	Fecha �ltima modificaci�n: 19-02-2016
##	Historial de cambios: 
##	19-02-2016	FJM		Primera versi�n
##
##**********************************************************************************************************************************
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;



/**
 * Clase que inicializa las configuraciones de los servicios WEB de SW 4.30
 * @author FreddyMolina
 * @creado 19/02/2016
 * @ultimamodificacion 19/02/2016
 * @version 1.0
 * @historial
 * 			19/02/2016 FJM Primera versi�n
 */
public class ServiciosClassLoader {
	
	//instancia de la clase ServiciosClassLoader
	private static ServiciosClassLoader instance = new ServiciosClassLoader();
	//propiedades que contiene las configuraciones iniciales
	private Properties propiedades = null;
	//archivo de propriedades de la clase ServiciosClassLoader
	private ResourceBundle rb = null;
	private static String mensajeError = null;

	//constructor
	protected  ServiciosClassLoader() {
		
		//inicializo las variables
		propiedades = new Properties();
		rb = ResourceBundle.getBundle("co.resources.ServiciosClassLoader", Locale.getDefault());
		
		//obtengo el nombre del archivo de propiedades
		String configFile = rb.getString("fileConfigurator");
		mensajeError = rb.getString("error.claveNOEncontrada");
		
		//variables para lectura de archivo de propiedades
		InputStream input = null;
		File file = new File(configFile);
		//directorio actual
		String dirActual="";
		
		try {
			
			//obengo el directorio actual
			dirActual= file.getCanonicalPath();

			//preparo la lectura de la informaci�n desde el archivo de propiedades
			input = new FileInputStream(configFile);
			
			//cargo la informaci�n desde el archivo de propiedades
			propiedades.load(input);
			
		} catch (FileNotFoundException e) {
			//archivo de propiedades no existe
			System.out.println(MessageFormat.format(rb.getString("error.archivoNoEncontrado"),dirActual));
		} catch (IOException e) {
			//error de lectura del archivo de propiedades
			System.out.println(rb.getString("error.lecturaPropiedades"));
		} finally {
			//si el archivo de propiedades esta abierto para lectura
			if (input != null) {
				try {
					//cierro el archivo de propiedades
					input.close();
				} catch (IOException e) {
					System.out.println(rb.getString("error.lecturaPropiedades"));
				}
			}
		}
		
	}

	
	
	/**
	 * funci�n que devuele una sola instancia de la clase ServiciosClassLoader
	 * @return
	 * 			Una �nica instancia de la clase ServiciosClassLoader
	 */
	public static ServiciosClassLoader getInstance(){
		if (instance == null)
			instance = new ServiciosClassLoader();
		return instance;
	}
	
	/**
	 * Funci�n que devuelve el conjunto de configuraciones iniciales
	 * @return
	 * 			Conjunto de configuraciones iniciales
	 */
	public static Properties getPropiedades() {
		
		//retorno el conjunto de propiedades
		return  getInstance().propiedades;
	}
	
	/**
	 * Funci�n que permite obtener el valor de una configuraci�n dado una clave
	 * @param clave
	 * 			Clave del atributo a buscar
	 * @return
	 * 			Valor del atributo ingresado. En caso de no existir lanza una Excepci�n
	 */
	public static String getAtributo(String clave) throws ServiciosException{
		
		//obtengo el valor del atributo seg�n la clave
		String valor = getInstance().propiedades.getProperty(clave);
		
		//Si la clave no existe
		if (valor == null){
			//la clave no existe, lanzo una excepci�n 
			throw new ServiciosException(MessageFormat.format(mensajeError,clave));
		}
		
		// retorno el valor de la clave
		return valor;
	}
	
	
	/**
	 * Funci�n que retorna el IP del servidor JBOSS
	 * @return
	 * 		IP del servidor JBOSS	
	 */
	public static String getJBOSS_IP(){
		return  getInstance().propiedades.getProperty("JBOSS_IP");
	}
	
	/**
	 * Funci�n que retorna el nombre DNS del servidor JBOSS
	 * @return
	 * 		Nombre DNS del servidor JBOSS	
	 */
	public static String getJBOSS_DNS(){
		return  getInstance().propiedades.getProperty("JBOSS_DNS");
	}
	
	/**
	 * Funci�n que retorna el nombre DNS del servidor JBOSS
	 * @return
	 * 		IP del servidor JBOSS
	 * 	
	 */
	public static String getJBOSS_PORT(){
		return  getInstance().propiedades.getProperty("JBOSS_PORT");
	}
	
	/**
	 * Funci�n que retorna la direcci�n del servidor JBOSS en el formato http://IP:8080
	 * @return
	 * 		Direcci�n del servidor JBOSS en el formato http://IP:8080
	 */
	public static String getJBOSS_SERVER_URL_BY_IP_DEFAULT(){
		return "http://"+getJBOSS_IP()+":"+getJBOSS_PORT();
	}
	
	/**
	 * Funci�n que retorna la direcci�n del servidor JBOSS en el formato http://DNS_NAME:8080
	 * @return
	 * 		Direcci�n del servidor JBOSS en el formato http://DNS_NAME:8080
	 */
	public static String getJBOSS_SERVER_URL_BY_DNS_DEFAULT(){
		return "http://"+getJBOSS_IP()+":"+getJBOSS_PORT();
	}
	
	/**
	 * Funci�n que retorna la direcci�n del servidor JBOSS en el formato http://IP
	 * @return
	 * 			Direcci�n del servidor JBOSS en el formato http://IP
	 */
	public static String getJBOSS_SERVER_URL_BY_IP(){
		return "http://"+getJBOSS_IP();
	}
	
	/**
	 * Funci�n que retorna la direcci�n del servidor JBOSS en el formato http://IP:PORT
	 * @param port
	 * 			Puerto de conexi�n al servidor JBOSS
	 * @return
	 * 			Direcci�n del servidor JBOSS en el formato http://IP:PORT
	 */
	public static String getJBOSS_SERVER_URL_BY_IP(String port){
		return "http://"+getJBOSS_IP()+":"+port;
	}
	
	/**
	 * Funci�n que retorna la direcci�n del servidor JBOSS en el formato http://DNS_NAME
	 * @return
	 * 			Direcci�n del servidor JBOSS en el formato http://DNS_NAME
	 */
	public static String getJBOSS_SERVER_URL_BY_DNS(){
		return "http://"+getJBOSS_IP();
	}
	
	/**
	 * Funci�n que retorna la direcci�n del servidor JBOSS en el formato http://DNS_NAME:PORT
	 * @param port
	 * 			Puerto de conexi�n al servidor JBOSS
	 * @return
	 * 			Direcci�n del servidor JBOSS en el formato http://DNS_NAME:PORT
	 */
	public static String getJBOSS_SERVER_URL_BY_DNS(String port){
		return "http://"+getJBOSS_IP()+":"+port;
	}
}
