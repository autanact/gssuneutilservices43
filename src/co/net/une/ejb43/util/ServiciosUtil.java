package co.net.une.ejb43.util;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: ServiciosUtil.java
##	Contenido: Clase que implementa librer�a de funcionalidades gen�ricas utilizadas dentro de los servicios
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Clase que implementa librer�a de funcionalidades gen�ricas utilizadas dentro de los servicios
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class ServiciosUtil {
	
	// Constantes de uso general
	// variable que representa mensaje de funcionamiento correcto de la operaci�n
	public static final String OK = "OK";
	// variable que representa mensaje de funcionamiento an�malo dentro de la operaci�n
	public static final String  KO = "KO";
	// variable que representa una consulta que no retorna resultados
	public static final String  SINRESULTADOS = "0";
	
	/**
	 * funci�n que valida si un campo es vac�o
	 * @param valorCampo
	 *            valor del campo
	 * @return verdadero si el campo es null o igual a string vac�o. Falso en
	 *         otro caso
	 */
	public static boolean esCampoVacio(Object valorCampo) {
		// valida si el campo tiene datos
		return valorCampo == null || "".equals(valorCampo);
	}
	
	/**
	 * funci�n que valida si un campo es vac�o
	 * @param valorCampo
	 *            valor del campo
	 * @return verdadero si el campo es null o igual a string vac�o. Falso en
	 *         otro caso
	 */
	public static boolean esCampoVacio(String valorCampo) {
		// valida si el campo tiene datos
		return valorCampo == null || "".equals(valorCampo);
	}
	
	
	/**
	 * funci�n que valida si la fecha en string cumple con el formato dado
	 * @param formato
	 * 			formato de fecha requerida
	 * @param fecha
	 * 			fecha a validar
	 * @throws ServiciosException
	 * 			Si la fecha no cumple con el formato, dispara una excepci�n
	 */
	public static void validarFecha(String formato, String fecha) throws ServiciosException {
        try {
        	@SuppressWarnings("unused")
			Date date = new SimpleDateFormat(formato).parse(fecha);
		} catch (ParseException e) {
			ServiciosException exception = new ServiciosException();
			throw exception;
		}	
	}

	/**
	 * Valida si un campo es de longitud v�lida
	 * 
	 * @param campo
	 *            Campo a validar
	 * @param maxLength
	 *            M�xima longitud del campo peritida
	 * @return verdadero si el campo cumple con la condici�n. Falso en cualquier
	 *         otro caso
	 */
	public static boolean esCampoLongitudValida(String campo, int maxLength) {
		// valida si el campo posee una longitud v�lida
		return campo != null && campo.length() <= maxLength;
	}

	/**
	 * Lanzador de excepciones utilzado cuando se presenta alguna falla gen�rica
	 * @throws ServiciosException
	 */
	public static void lanzarServicioException() throws ServiciosException{

		ServiciosException exception = new ServiciosException();
		throw exception;
	}
	
	/**
	 * Lanzador de excepciones utilzado cuando se presenta alguna falla espec�fica, denotada en el mensaje
	 * @param mensaje
	 * 			Mensaje informativo de la excepci�n
	 * @throws ServiciosException
	 */
	public static void lanzarServicioException(String mensaje) throws ServiciosException{

		ServiciosException exception = new ServiciosException(mensaje);
		throw exception;
	}
	
	/**
	 * Permite depurar los par�metros que vienen en el HashMap
	 * 
	 * @param respuesta
	 *            HashMap que contiene el conjunto de par�metros
	 * @param nombreParametro
	 *            nombre del par�metro de depurar
	 * @return
	 */
	public static String depurarParametro(Map<String, Object> respuesta,String nombreParametro) {
		return ((String) respuesta.get(nombreParametro) == null ? "": (String) respuesta.get(nombreParametro));
	}
	
	/**
	 * Funci�n que retorna que mensaje de error de par�metros inv�lidos configurado en las configuraciones iniciales
	 * @return
	 * 		mensaje de error de par�metros inv�lidos configurado en las configuraciones iniciales
	 */
	public static String getMensajeErrorParametros(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("validate.parameterException");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna que mensaje de error de par�metros obligatorios configurado en las configuraciones iniciales
	 * @return
	 * 		mensaje de error de par�metros obligatorios configurado en las configuraciones iniciales
	 */
	public static String getMensajeErrorParametrosObligatorios(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("validate.required.parameterException");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna un mensaje de error para identificar una excepci�n GSS general configurado en las configuraciones iniciales
	 * @return
	 * 		mensaje de error para identificar una excepci�n GSS general configurado en las configuraciones iniciales
	 */
	public static String getMensajeErrorGSS(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("validate.GSSException");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna un mensaje de error para identificar una excepci�n general configurado en las configuraciones iniciales
	 * @return
	 * 		mensaje de error para identificar una excepci�n general configurado en las configuraciones iniciales
	 */
	public static String getMensajeErrorGeneral(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("validate.generalException");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el formato de fecha por defecto - ddMMyyyy
	 * @return
	 * 		formato de fecha por defecto - ddMMyyyy
	 */
	public static String getFormatoFechaDefault(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("validate.dateFormat");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el URL del cliente de GEOrreferencia Externo
	 * @return
	 * 		URL del cliente de GEOrreferencia Externo
	 */
	public static String getURLClienteGeorreferencia(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("serviciosGSS.clienteGEO");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el URL del cliente de GEOrreferencia Externo
	 * @return
	 * 		URL del cliente de GEOrreferencia Externo
	 */
	public static String getURLClienteEnviarCRM(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("serviciosGSS.clienteCRM");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	
	/**
	 * Funci�n que retorna el URL del servidor donde est� publicados los servicios
	 * @return
	 * 		 URL del servidor donde est� publicados los servicios
	 */
	public static String getURLServidorServicios(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("serviciosGSS.host");
		} catch (ServiciosException e) {}
		return mensaje;
	}

	
	/**
	 * Funci�n que retorna el nombre del ambiente donde se est�n ejecutando los servicios
	 * @return
	 * 		 nombre del ambiente donde se est�n ejecutando los servicios
	 */
	public static String getNombreAmbiente(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("serviciosGSS.ambiente");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el archivo de configuraci�n del motor de georreferencia masivo
	 * @return
	 * 		 archivo de configuraci�n del motor de georreferencia masivo
	 */
	public static String getMotorGeoConfigFile(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("motorGeo.configFile.path");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el archivo de configuraci�n del motor de generaci�n de lotes
	 * @return
	 * 		 archivo de configuraci�n del motor de generaci�n de lotes
	 */
	public static String getMotorLoteConfigFile(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("motorLotes.dir.path");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el archivo de configuraci�n del motor de generaci�n de hist�rico de direcciones excepcionadas
	 * @return
	 * 		 archivo de configuraci�n del motor de generaci�n de hist�rico de direcciones excepcionadas
	 */
	public static String getMotorHistoricoConfigFile(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("motorHistorico.configFile.path");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	
	/**
	 * Funci�n que retorna la variable para configurar la activaci�n de los servicios
	 * @return
	 * 		 archivo de configuraci�n del motor de generaci�n de hist�rico de direcciones excepcionadas
	 */
	public static String getJobActivos(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("job_activar");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el nombre del usuario que ejecuta los JOBS
	 * @return
	 * 		nombre del usuario que ejecuta los JOBS
	 */
	public static String getJobUsuario(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("job_usuario");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	
	/**
	 * Funci�n que retorna el tama�o de paginador de los filtros de GDE
	 * @return
	 * 		 tama�o de paginador de los filtros de GDE
	 */
	public static String getTamanoPaginador(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("tamano_paginador");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el tiempo en minutos de espera de los servicios
	 * @return
	 * 		 tiempo en minutos de espera de los servicios
	 */
	public static String getTimeOutServicios(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("tiempo_tiemeout");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	/**
	 * Funci�n que retorna el directorio donde se van a guardar los archivos de lotes
	 * @return
	 * 		 directorio donde se van a guardar los archivos de lote
	 */
	public static String getDirectorioGuardarLote(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("file_path");
		} catch (ServiciosException e) {}
		return mensaje;
	}
	
	
	/**
	 * Funci�n que retorna el tiempo de espera para que se activen los JOBS cuando se levante el JBOSS
	 * @return
	 * 		 tiempo de espera para que se activen los JOBS cuando se levante el JBOSS
	 */
	public static String getTiempoEsperaActivarJobs(){
		String mensaje ="";
		try {
			//obtengo el mensaje de las configuraciones iniciales
			mensaje= ServiciosClassLoader.getAtributo("job_activar.segundosEspera");
		} catch (ServiciosException e) {}
		return mensaje;
	}
}
